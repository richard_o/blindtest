var app = require('express')();
var http = require('http').Server(app);
var express = require('express');
var nunjucks  = require('nunjucks');
var wss = require('./websocket-server');
var request = require("request")

nunjucks.configure('views', {
  autoescape: true,
  express   : app
});

app.set('view engine', 'nunjucks');

// DATA TEST
var dashboardData =
{
  score: '200',
  playersList : [
    { player :
      {
        playerId: '1',
        name: 'Player name',
        score: '30'
      }
    },
    { player :
      {
        playerId: '2',
        name: 'Player name',
        score: '30'
      }
    },
    { player :
      {
        playerId: '3',
        name: 'Player name',
        score: '30'
      }
    }
  ]
};

// var questions =
// {
//   theme: '200',
//   themeBackground : 's',
//   questionsList : [
//     { question :
//       {
//         questionId: '1',
//         questionType: 'buzzer',
//         questionAnswer: '2'
//       }
//     },
//     { question :
//       {
//         questionId: '2',
//         questionType: 'qcm',
//         questionAnswer: '2'
//       }
//     }
//   ]
// };

// console.log('-----------------------GAME DATA-------------------------');
// console.log(dashboardData);
// console.log('-----------------------GAME DATA-------------------------');
// console.log('-----------------------QUESTIONS-------------------------');
// console.log(questions);
// console.log('-----------------------QUESTIONS-------------------------');

var questions = 'https://dl.dropboxusercontent.com/u/3077895/blindtest-project/game-data.json';

request({
    url: questions,
    json: true
}, function (error, response, body) {
    // console.log(error);
    // console.log(body);
    // console.log(response);

    if(response.statusCode === 404){
      console.log('ERROR', error);
    }

    console.log(response.statusCode);

    if (!error && response.statusCode === 200) {
      // console.log(body);
      return body;
    }
});

var toto = request(questions, function(error, response, body){
  console.log('ok');
})

console.log(toto.callback());
//Allow static assets
app.use(express.static('assets'));

// Displaying the Dashboard
app.get('/dashboard', function(req, res) {
  res.render('dashboard.html', dashboardData);
});

// Displaying on player smartphone
app.get('/', function(req, res) {
  res.render('player.html');
})

// Displaying on the admin smartphone
app.get('/admin', function(req, res) {
  res.render('admin.html');
})

http.listen(3000, function() {
    console.log('--------- Server is listening on *:3000 ---------------');
})
