# README

## What is this repository for?

This repository is an easy to use blindtest.

Here the different functionalities:

- Multi-player
- Easy to add questions
- Easy to install
- Handle music, images and videos
- A full dashboard which display :

  - The current question on the main screen (with image, video or a specific image for a the music)
  - The results screen with the response (Text for vieo and image or image if the media is a track)
  - A counter which can be change
  - The number of player
  - The name (or nickname) of the different player
  - The score of each player
  - The number of the current question and the total number of question

## How do I get set up?

1) Npm install<br>
2) Npm start and enjoy

## Who do I talk to?

- Repo owner or admin

## To-do List :

**Main functionalities**

- Main Language : English
- Do a unit test for the data part test.
- Use Sessions storage or localStorage in order to identify the player
- NOT MVP : Multilanguages
- NOT MVP : Try the blindtest in a server using heroku

**Contribution and administration part :**

- Contributeur can choose the type of question : open response or single choice question
- Contributor can choose the time for the counter (one choice for all the quiz)
- Contributor can have multiple quiz and can launch which one he wants
- For music : he can add a youtube music
- For video : he can add a youtube link
- For image : he can add a google image link -

**Design and integration part:**

- Do the front with HTML & CSS first
- and after integrate it with ReactJS

**The Dashboard**

- Dashboard must have a the current question on the main screen (with image, video or a specific image for a the music) and the results screen (Text for video and image or image if the media is a track)
- Dashboard must be responsive
- Dashboard must display the number of player
- Dashboard must display the name of nickname of each player
- Dashboard must display the score of each player
- Dashboard must display a configurable counter
- Dashboard must triggered the first person who has buzzed or send the right answer
- Dashboard must show the number of player
- Dashboard must show the number of question and the total amount of question
- Dashboard must show the number of player

**On the Player smartphone**

- Player can add his nickname: if not nickname, choose a nickname + number
- Player interface must have a buzzer or a liste of choice

**On the Administrator smartphone**

- Administrator can see all the player in order to choose the winner of the question
- Administrator can stop the game if needed

**INFOS**<br>
<http://codular.com/node-web-sockets> : using only websockets no socket IO.

GREAT RESSOURCES ===>>>>>

- <https://blog.bini.io/developper-une-application-avec-socket-io/>
- <https://www.websocket.org/echo.html>
- <http://blog.viseo-bt.com/rex-node-js-dans-un-projet-front/>
- <http://miageprojet2.unice.fr/Intranet_de_Michel_Buffa/HTML5_pour_la_L3_Pro_(2013)/TP_WebSockets%3A_%C3%A9criture_d'un_programme_de_chat_en_HTML5>
-
